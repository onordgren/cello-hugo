// dato.config.js

module.exports = (dato, root, i18n) => {
  dato.categories.forEach((category, index) => {
    createCategory(category, index, root, i18n)
    createProducts(category, dato, root, i18n)
  });
  createIndex(dato.homePage, root, i18n);
  createPage(dato.aboutPage, 'about', 1, root, i18n);
  createPage(dato.contactPage, 'contact', 2, root, i18n);
  root.addToDataFile('config.toml', 'toml', { params: { socialMediaProviders: getProviders(dato.socialMediaIcon.provider.toMap()) } });
};

function createIndex(page, root, i18n) {
  root.directory('content', (contentDir) => {
    i18n.availableLocales.forEach((locale) => {
      i18n.withLocale(locale, () => {
        contentDir.createPost(
          `_index.${locale}.md`, "yaml", {
            frontmatter: {
              title: page.title,
              lastmod: page.updatedAt,
              metaTags: page.metaTags ? page.metaTags.toMap() : {},
            },
            content: page.description || ""
          }
        );
      });
    });
  });
}

function createPage(page, layout, weight, root, i18n) {
  const slug = page.slug;
  root.directory(`content/${slug}`, (pageDir) => {
    i18n.availableLocales.forEach((locale) => {
      i18n.withLocale(locale, () => {
        pageDir.createPost(
          `_index.${locale}.md`, "yaml", {
            frontmatter: {
              title: page.title,
              navigationTitle: page.navigationTitle,
              url: `${getLocalePath(locale)}/${page.slug}/`,
              image: page.image && {
                url: page.image.url({ w: 300, auto: ['compress', 'enchance', 'format'] }),
                title: page.image.title,
                alt: page.image.alt,
              },
              menu: 'page',
              weight,
              layout,
              page: page.toMap(),
              lastmod: page.updatedAt,
              metaTags: page.metaTags ? page.metaTags.toMap() : {},
            },
            content: page.description || ""
          }
        );
      });
    });
  });
}

function createCategory(category, index, root, i18n) {
  root.directory(`content/${category.slug}`, (categoryDir) => {
    i18n.availableLocales.forEach((locale) => {
      i18n.withLocale(locale, () => {
        categoryDir.createPost(
          `_index.${locale}.md`, "yaml", {
            frontmatter: {
              title: category.title,
              navigationTitle: category.navigationTitle,
              information: category.information,
              url: `${getLocalePath(locale)}/${category.slug}/`,
              menu: 'category',
              image: getCategoryImage(category),
              weight: index + 1,
              lastmod: category.updatedAt,
              metaTags: category.metaTags ? category.metaTags.toMap() : {},
            },
            content: category.description || ""
          }
        );
      });
    });
  });
}

function createProducts(category, dato, root, i18n) {
  const products = dato.products.filter(product => product.category.id === category.id);

  products.forEach((product) => {
    const slug = product.slug;
    root.directory(`content/${category.slug}`, (productDir) => {
      i18n.availableLocales.forEach((locale) => {
        i18n.withLocale(locale, () => {
          productDir.createPost(
            `${slug}.${locale}.md`, "yaml", {
              frontmatter: {
                title: product.title,
                url: `${getLocalePath(locale)}/${category.slug}/${product.slug}/`,
                images: getImages(product.images),
                frontImage: getFrontImage(product.images),
                price: getPrice(product),
                lastmod: product.updatedAt,
                information: product.information,
                rssImage: getRssImage(product.images),
                metaTags: product.metaTags ? product.metaTags.toMap() : {},
                archived: !!product.archived,
              },
              content: product.description || ""
            }
          );
        });
      });
    });
  });
}

function getCategoryImage(category) {
  return category.image ? {
    url: category.image.url({ w: 120, auto: ['compress', 'enchance', 'format'] }),
    alt: category.image.alt,
    title: category.image.title,
    width: 120,
    height: 180,
  } : '';
}

function getPrice(product) {
  return parseInt(product.price);
}

function getProviders(providers) {
  return providers.map(provider => `${provider.name},${provider.link}`);
}

function getImages(images) {
  return images.map(image => ({
    thumb: image.url({ fit: 'crop', h: 240, w: 160, auto: ['compress', 'enchance', 'format'] }),
    full: image.url(),
    alt: image.alt,
    title: image.title,
    width: 160,
    height: 240,
  }));
}

function getFrontImage(images) {
  return images.length > 0 ? {
    url: images[0].url({ fit: 'crop', w: 220, h: 220, auto: ['compress', 'enchance', 'format'] }),
    alt: images[0].alt,
    title: images[0].title,
    width: 220,
    height: 220,
  } : '';
}

function getRssImage(images) {
  return images.length > 0 ? images[0].url({ fit: 'crop', h: 240, w: 160, auto: ['compress', 'enchance', 'format'] }) : '';
}

function getLocalePath(locale) {
  return locale === 'en' ? '' : `/${locale}`;
}
