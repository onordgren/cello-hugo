export default function(callback) {
  document.addEventListener("DOMContentLoaded", callback);
}
